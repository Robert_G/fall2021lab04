public class FibonacciSequence extends Sequence {
    private int firstN;
    private int secondN;

    public FibonacciSequence(int firstN, int secondN){
        this.firstN = firstN;
        this.secondN = secondN;
    }

    public int getTerm(int n){
        int num1 = this.firstN;
        int num2 = this.secondN;
        int res = 0;
        for(int i = 2; i < n; i++)
        {
            res = num1 + num2;
            num1 = num2;
            num2 = res;
        }
        return res;
        
    }

    

}
