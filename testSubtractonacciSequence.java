import static org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
public class testSubtractonacciSequence {
    @Test
    public void getTermTest() {
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 4);
        sub.getTerm(3);
        assertEquals(-2, sub.getTerm(3));
    }
}
