//Junit imports
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class testFibonacciSequence {

    //Test
    @Test
    public void getTermTest(){
        FibonacciSequence fib = new FibonacciSequence(4, 6);
        assertEquals(16, fib.getTerm(4));
    }
}
