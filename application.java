public class application {
    public static void main(String[] args){
        String s  = "Fib;1;1;x;Sub;2;4;x;Fib;2;3;x";
        Sequence[] sequences = parse(s);
        print(sequences, 5);
    }

    public static Sequence[] parse(String s) {
        String[] stringList = s.split(";");
        Sequence[] sequences = new Sequence[(stringList.length)/4];
        int indexPosition = 0;

        for(int i = 0; i < stringList.length; i = i+4){
            if(stringList[i].equals("Fib") || stringList[i].equals("fib")){         
                int num1 = Integer.parseInt(stringList[(int)i+1]);
                int num2 = Integer.parseInt(stringList[(int)i+2]);
                Sequence fibSequence = new FibonacciSequence(num1, num2);
                sequences[indexPosition] = fibSequence;
            }
            else{
                int num1 = Integer.parseInt(stringList[(int)i+1]);
                int num2 = Integer.parseInt(stringList[(int)i+2]);
                Sequence subFibSequence = new SubtractonacciSequence(num1, num2);
                sequences[indexPosition] = subFibSequence;
            }

            indexPosition++;
        }
        return sequences;
        
    }

    public static void print(Sequence[] sequences, int n){
        for(int i=0; i<sequences.length; i++){
            System.out.println(sequences[i].getTerm(n));
        }
    }
}
