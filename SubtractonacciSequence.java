public class SubtractonacciSequence extends Sequence{

    //Fields
    private int num1;
    private int num2;

    //Constructor
    public SubtractonacciSequence(int num1, int num2){
        this.num1 = num1;
        this.num2 = num2;
    }
    
    //Return number at given position in fibonacci sequence
    public int getTerm(int position){
        int num1 = this.num1;
        int num2 = this.num2;
        int result = 0;

        //Check if position to fetch is 0, 1, 2, or anything greater than 2
        if(position==0){
            return 0;
        }
        else if(position==1){
            return num1;
        }
        else if(position==2){
            return num2;
        }
        else{
            for(int i=2; i<position; i++){
                result = num1 - num2;
                num1 = num2;
                num2 = result;
            }
        }

        return result;
    }

}